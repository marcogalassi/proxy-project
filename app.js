'use strict'

var express = require('express');
var bodyParser = require('body-parser');
var url = require('url');
var psl = require('psl');
var Joi = require('joi');
var normalizeUrl = require('normalize-url');
var redis = require('redis');
var redisConf = require("./config/redis-conf.json");
var redisClient = redis.createClient(redisConf);

const uriValidation = Joi.object().keys({
  domain: Joi.string().trim().max(253).required()
});

const maxConnectionsValidator = Joi.object().keys({
  maxConnections: Joi.number().integer().min(10).max(2048).required()
});

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));

var router = express.Router();
router.get('/whitelist', (req, res, next) => {
  redisClient.smembers('whitelist', (err, reply) => {
    if (err) {
      return res.status(500);
    }
    res.status(200).json({
      whitelist: reply.map(el => {
        return {
          id: el,
          href: '/admin/whitelist/' + el
        }
      })
    });
  });
});

router.get('/whitelist/:id', (req, res, next) => {
  redisClient.sismember('whitelist', req.params.id, (err, reply) => {
    if (err) {
      return res.status(500);
    }
    if (reply == 0) {
      return res.sendStatus(404);
    }
    res.status(200).json({
      id: req.params.id
    });
  });
});

router.post('/whitelist', (req, res, next) => {
  let value = uriValidation.validate(req.body);
  if (value.error) {
    return res.status(400).send('domain is required');
  }
  try {
    value = normalizeUrl(value.value.domain, {
      stripWWW: false
    });
    value = psl.parse(url.parse(value).hostname);
  } catch (err) {
    return res.status(400).send('invalid domain');
  }

  redisClient.sadd('whitelist', value.domain, (err, reply) => {
    if (err) {
      return res.sendStatus(500);
    }
    res.status(200).json({
      id: value.domain
    });
  });
});

router.delete('/whitelist/:id', (req, res, next) => {
  redisClient.srem('whitelist', req.params.id, (err, reply) => {
    if (err) {
      return res.status(500);
    }
    if (reply == 0) {
      return res.sendStatus(404);
    }
    res.send('Element removed: ' + reply);
  });
});

router.get('/maxconnections', (req, res, next) => {
  res.json({
    maxConnections: parseInt(req.app.get('proxy').maxConnections)
  });
});

router.post('/maxconnections', (req, res, next) => {
  let obj = maxConnectionsValidator.validate(req.body);
  if (obj.error) {
    return res.status(400).send('Invalid Request');
  }
  req.app.get('proxy').maxConnections = obj.value.maxConnections;
  redisClient.set('proxymaxconnections', obj.value.maxConnections, (err, reply) => {
    if (err) {
      return res.sendStatus(500);
    }
    res.sendStatus(200);
  });
});

app.use('/admin', router);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function (err, req, res, next) {
  // render the error page
  res.status(err.status || 500);
  res.send(err.message || 'error');
});

module.exports = app;