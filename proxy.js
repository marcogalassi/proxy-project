'use strict'

var express = require('express');
var http = require('http');
var url = require('url');
var psl = require('psl');
var normalizeUrl = require('normalize-url');
var redis = require('redis');
var redisConf = require("./config/redis-conf.json");
var redisClient = redis.createClient(redisConf);

var proxy = express();
proxy.set('redis', redisClient);

proxy.use((creq, cres, next) => {

  // remove specific headers (see: https://www.mnot.net/blog/2011/07/11/what_proxies_must_do)
  delete creq.headers['TE'];
  delete creq.headers['Transfer-Encoding'];
  delete creq.headers['Keep-Alive'];
  delete creq.headers['Proxy-Authorization'];
  delete creq.headers['Proxy-Authentication'];
  delete creq.headers['Trailer'];
  delete creq.headers['Upgrade'];

  let value, parsed, domain;

  try {
    value = normalizeUrl(creq.url, {
      stripWWW: false
    });
    parsed = url.parse(value);
    domain = psl.parse(parsed.hostname);
  } catch (err) {
    return cres.status(400).send('invalid hostname');
  }

  // whitelist lookup of target hostname
  redisClient.sismember('whitelist', domain.domain, (err, reply) => {
    // if hostname is NOT there
    if (reply == 0) {
      let err = new Error('Domain not allowed');
      err.status = 403;
      return next(err);
    }
    // if it's there
    let opts = {
      method: creq.method,
      host: parsed.host,
      path: parsed.path,
      headers: creq.headers
    };
    // create request to target destination
    let targetRequest = http.request(opts, (targetResponse) => {
      /**
       * As the target responses, stream the response to the 
       * original client without touching it.
       */
      targetResponse.on('data', chunk => {
        cres.write(chunk);
      });
      targetResponse.on('end', chunk => {
        cres.end();
      });
    });
    /**
     * On 'data' events (incoming chunk of data), stream the chunks to the target destination
     */
    creq.on('data', (chunk) => {
      targetRequest.write(chunk);
    });
    creq.on('end', (chunk) => {
      targetRequest.end();
    });
    targetRequest.on('error', err => {
      /**
       * Here we just pass the error to the next error middleware. In reality this could and should
       * be improved by handling the different possible errors.
       * Examples:
       * General error, i.e.
       *   - ECONNRESET - server closed the socket unexpectedly
       *   - ECONNREFUSED - server did not listen
       *   - HPE_INVALID_VERSION
       *   - HPE_INVALID_STATUS
       *   - ... (other HPE_* codes) - server returned garbage
       */
      return next(err);
    });
  })
});

// error handler
proxy.use(function (err, req, res, next) {
  // render the error page
  res.status(err.status || 500).send(err.message);
});

module.exports = proxy;