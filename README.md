## Proxy

This repo is a simple proposed solution to a techinical interview. This project is a simple bare bones HTTP forward proxy written in Node.js.

It contains a lot of flaws and should definetly been improved adding more features, tests, authentication and so on.

Download or clone this repo, `cd`  into the folder and run `npm install`.

Run the app using `node bin/www`. The app is divided in 2 parts on purpose because they are conceptually different: one is the actual proxy, the other is the admin API server.

The proxy is listening on port `3001`. This is a simple HTTP proxy, that has been tested with [Insomnia](https://insomnia.rest/). Check out how to set the proxy in Insomnia [here](https://support.insomnia.rest/article/53-http-s-proxy), in short _Application->Preferences->HTTP Network Proxy_, check **Enable Proxy** and add _localhost:3001_ to **HTTP Proxy**. Also add _localhost, 127.0.0.1_ to **No Proxy**.

The proxy behaviour is to deny all the traffic to any destination by default. In order to selectively allow traffic to certain destinations, use the API server to add and remove domains from a whitelist.

To test the proxy, try to reach http websites, for instance: `www.trenitalia.com`, `www.bbc.com`.

By default, the proxy can handle `1024` concurrent connections. It is possible to hot-change this number using the admin APIs (see below).

The API server is listening on port `3000` and allows hot changes in the proxy configuration, eg it allows to retrive, add and remove a elements to the proxy whitelist.

## Redis installation

This project uses [Redis](https://redis.io/) to store the whitelist and the connection limit. In particular, I have used a cloud based implementation for simplicity, but any other installation (self hosted or cloud based) can be used. In order to connect to redis, use the configuration file `config/redis-conf.json`.

The connection information found in the file are relative to a redis cloud installation on [redislab](https://redislab.com). The free tier on redislab does not allow to set up any kind of data persistence, but this could be done in a local installation, or with a paid plan on redislab. Feel free to use it as you prefer, it is an anonymous installation and is using just the free tier.

## Admin APIs

The API server is listening on port 3000. Here are listed the available APIs.
The API responses are JSON objects.

### `GET /admin/whitelist`

Shows the content of the whitelist as a JSON list.

### `GET /admin/whitelist/:id`

Shows a specific element of the whitelist corresponding to `:id`.
Example: `/admin/whitelist/bbc.com`


Response: `200 OK` if the resource exists or `404 Not Found` if the resource does not exist.

### `POST /admin/whitelist`

Allows to add a domain to the whitelist. This API accepts a JSON object like follows:

```
{
  "domain": "yourdomain"
}
```

The domain `youdomain` must be a valid domain: eventual protocol, subdomains, URL path and query parameters are stripped, and only the actual domain is saved. For instance, if you POST the following body:

```
POST /admin/whitelist
{
  "domain": "http://www.mywebsite.com/some/path?foo=bar&baz=qux"
}
```

what will be saved is only `mywebsite.com`, allowing all the traffic to that domain without caring about subdomains etc. This a choice that could be changed.

### `DELETE /admin/whitelist/:id`

Allows to remove an element from the whitelist specifying its id (domain).
Example:

```
DELETE /admin/whitelist/mywebsite.com
```

Response: `200 OK` if the resource exists and is deleted, `404 Not Found` if the resource does not exit.

### `GET /admin/maxconnections`

Retrive the current maximum number of concurrent connections to the proxy.

### `POST /admin/maxconnections`

Set a new limit.

```
{
  "maxConnections": value
}
```

The value must be an integer between 10 and 2048.